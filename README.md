# AA Team12

repo for team 12 for the 2021W course of Analytics & Applications

## Setup

.csv files are gitignored.

unzip the csvs.zip file in the csvs folder.

use conda to create an environment using the Team12.yml file, or install the necessary packages yourself.

## csvs

philadelphia 201X.csv and hourly weather data was provided to us.

indego-trips-2018-qX.csv are from indegos website and were used to find out the geo locations of the stations

p.csv and p1X.csv were created using the data_cleaning file.

weather_hourly_philadelphia.csv includes the weather data provided to us.

## notebooks

#### Part 1 Data collection and preparation

data_cleaning.ipynb <= cleans the data

#### Part 2 Descriptive analytics

temporal.ipynb <= temporal demand paterns

geo_demand.ipynb <= geographic demand patterns

coverage.ipynb <= geographic demand and coverage KPI

revenue.ipynb <= estimates revenue (KPI)

ticket_distribution.ipynb <= shows data on subscriptions (KPI)

utilization.ipynb <= shows utilization of the bike fleet (KPI)

#### Part 3 Cluster Analysis

clustering.ipynb <= cluster analysis

#### Part 4 Predictive Analytics

prediction.ipynb <= predictive analysis


## Other

Some code fields will take a while to run. If that is the case, the will be a comment in the code with a time estimate. 

This estimate was made on a higher end machine, so your compute times might be significantly longer